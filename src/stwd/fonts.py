from typing import List

from matplotlib import font_manager
from matplotlib import pyplot as plt
from matplotlib import rcParams


def select_font(requested_fonts: List[str]) -> str:
    """Select the right font for matplotlib based on availability

    Args:
        fonts: A list of preferred fonts

    Returns:
        The first available font. If none exists, use Dejavu Sans.
    """

    for font in requested_fonts:
        if font.lower().replace(" ", "") == "dejavusans":
            return "DejaVu Sans"
        elif "DejaVuSans" not in font_manager.findfont(font):
            return font

    return "Dejavu Sans"

def setup_font() -> None:
    """Setup matplotlib for style conforming to Storytelling with Data

    Configurations:
    - Set matplotlib to use Arial or Liberation Sans fonts. Fallbacks to Dejavu Sans.
    """
    # configure plot font family
    plt.rcParams['font.family'] = select_font(['Arial', 'Liberation Sans'])

    # configure mathtext bold and italic font family
    rcParams['mathtext.fontset'] = 'custom'
    rcParams['mathtext.bf'] = select_font(
        ['Arial:bold', 'Liberation Sans:bold'])
    rcParams['mathtext.it'] = select_font(
        ['Arial:italic', 'Liberation Sans:italic'])


"""Font sizes"""
SIZE_SLIDE_TITLE = 26
SIZE_GRAPH_TITLE = 17

SIZE_LABEL_LARGE = 20
SIZE_LABEL = 14

SIZE_NOTE_LARGE = 17
SIZE_NOTE = 14

SIZE_FOOTNOTE = 10.5
