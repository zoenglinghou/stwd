#!/usr/bin/env python

from distutils.core import setup

setup(name='stwd',
      version='0.1',
      description='Storytelling with Data Python utils',
      author='Linghao Zhang',
      author_email='linghao.zhang@protonmail.com',
      url='',
      packages=['stwd'],
     )